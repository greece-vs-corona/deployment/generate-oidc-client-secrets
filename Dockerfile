FROM busybox

WORKDIR /app

COPY generate_secrets.sh /app/

RUN chmod +x /app/generate_secrets.sh
CMD ["/app/generate_secrets.sh"]
