#!/bin/sh

cat > ${SECRETS_DIR}client_secrets.json <<EOF
{
    "web": {
        "issuer": "https://${GITLAB_URL}",
        "client_id": "${GITLAB_CLIENT_ID}",
        "client_secret": "${GITLAB_CLIENT_SECRET}",
        "auth_uri": "https://${GITLAB_URL}/oauth/authorize",
        "redirect_uris": [
            "${GITLAB_REDIRECT_URI}"
        ],
        "userinfo_uri": "https://${GITLAB_URL}/oauth/userinfo",
        "token_uri": "https://${GITLAB_URL}/oauth/token",
        "token_introspection_uri": "https://${GITLAB_URL}/oauth/introspect"
    }
} 
EOF
